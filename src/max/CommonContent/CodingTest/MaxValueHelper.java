package max.CommonContent.CodingTest;

/**
 *
 * @author serdarozay
 */
public class MaxValueHelper implements IFindTopValues{

    @Override
    public int FindMaxValue(int[] unorderedValues) throws Exception {
        if (unorderedValues == null || unorderedValues.length < 1)
            throw new Exception("Input array is null or empty!");
        
        int maxvalue  = unorderedValues[0];
        for(int i = 0; i< unorderedValues.length;i++)
        {
            if(unorderedValues[i] > maxvalue)
            {
                maxvalue = unorderedValues[i];
            }
        }
        return maxvalue;
    }
    
    @Override
    public int[] FindTopNValues(int[] unorderedValues, int n) throws Exception {
        if(n < 1)
            throw new Exception("n value must be greater than 1");
        
        int pivot = unorderedValues[0];
        int[] tmpMaxArray = new int[unorderedValues.length];
        int[] tmpMinArray = new int[unorderedValues.length];
        int minIndex = 0;
        int maxIndex = 0;
        
        for(int i= 0; i< unorderedValues.length;i++)
        {
            if(unorderedValues[i] > pivot)
            {
                tmpMaxArray[maxIndex] = unorderedValues[i];
                maxIndex++;
            }else
            {
                tmpMinArray[minIndex] = unorderedValues[i];
                minIndex++;
            }   
        }
        if(maxIndex == n)
        {
            return GetFirstNElement(tmpMaxArray, maxIndex);
        }
        else if (maxIndex == n-1)
        {
            return MergeArrays(GetFirstNElement(tmpMaxArray, maxIndex),new int[] { pivot });
        }
        else if(maxIndex == 0)
        {
            unorderedValues[0] = unorderedValues[unorderedValues.length -1];
            unorderedValues[unorderedValues.length -1] = pivot;
            return FindTopNValues(unorderedValues, n);
        }
        else if(maxIndex > n)
        {
            return FindTopNValues(GetFirstNElement(tmpMaxArray, maxIndex),n);
        }else
        {
            int newTargetN = n - maxIndex;
            int [] mergeSecondArray  = FindTopNValues(GetFirstNElement(tmpMinArray, minIndex), newTargetN);
            return MergeArrays(GetFirstNElement(tmpMaxArray,maxIndex), mergeSecondArray);
        }
    }

    private int[] MergeArrays(int[] first, int[] second) {
        int[] result = new int[first.length + second.length];
        for(int i = 0;i < result.length;i++)
        {
            if(i < first.length)
                result[i] = first[i];
            else
                result[i] = second[i-first.length];
        }
        return result;
    }
    
    private int[] GetFirstNElement(int[] array,int n)
    {
        int [] result = new int[n];
        for(int i= 0;i < n;i++)
        {
            result[i] = array[i];
        }
        return result;
    }
}
