package max.CommonContent.CodingTest;

/**
 *
 * @author serdarozay
 */
public interface IFindTopValues {
    int FindMaxValue( int[] unorderedValues ) throws Exception;
    int[] FindTopNValues( int[]unorderedValues, int n ) throws Exception;
}
