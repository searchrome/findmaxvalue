/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package max.CommonContent.CodingTest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author serdarozay
 */
public class MaxValueHelperTest {
    
    public MaxValueHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of FindMaxValue method, of class MaxValueHelper.
     */
    @Test
    public void testFindMaxValue() throws Exception {
        System.out.println("FindMaxValue");
        int[] unorderedValues = new int[] {8,7,8,-5,12,7,34,2,9,23,17};
        MaxValueHelper instance = new MaxValueHelper();
        int expResult = 34;
        int result = instance.FindMaxValue(unorderedValues);
        assertEquals(expResult, result);
    }
    
     @Test
    public void testFindMaxValueWithOnNegative() throws Exception {
        System.out.println("testFindMaxValueWithOnNegative");
        int[] unorderedValues = new int[] {-5,-6,-12,-4,-32,-2};
        MaxValueHelper instance = new MaxValueHelper();
        int expResult = -2;
        int result = instance.FindMaxValue(unorderedValues);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFindMaxValueBigValue() throws Exception {
        System.out.println("testFindMaxValueWithOnNegative");
        int[] unorderedValues = new int[] {-5,-6,-12,-4,-32,-2};
        MaxValueHelper instance = new MaxValueHelper();
        int expResult = -2;
        int result = instance.FindMaxValue(unorderedValues);
        assertEquals(expResult, result);
    }

    /**
     * Test of FindTopNValues method, of class MaxValueHelper.
     */
    @Test
    public void testFindTopNValues() throws Exception {
        System.out.println("FindTopNValues");
        int[] unorderedValues = new int[] {8,7,5,7,7,12,7,34,2,9,23,17};
        int n = 3;
        MaxValueHelper instance = new MaxValueHelper();
        int[] expResult =  new int[] {34,23,17};
        int[] result = instance.FindTopNValues(unorderedValues, n);
        assertArrayEquals(expResult, result);
    }
    
    
}
